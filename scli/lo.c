/* Dooba SDK
 * Digital Input / Output Library
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "dio.h"
#include "scli/lo.h"

// Set Pin Low
void dio_scli_lo(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	uint8_t p;

	// Acquire Pin Number
	if(str_next_arg(args, args_len, &x, &l) == 0)			{ scli_printf("Missing pin\n"); return; }
	p = strtoul(x, 0, 0);

	// Set Pin
	scli_printf("Setting %i low...\n", p);
	dio_lo(p);
}
