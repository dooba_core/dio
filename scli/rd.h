/* Dooba SDK
 * Digital Input / Output Library
 */

#ifndef	__DIO_SCLI_RD_H
#define	__DIO_SCLI_RD_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Read Pin
extern void dio_scli_rd(char **args, uint16_t *args_len);

#endif
