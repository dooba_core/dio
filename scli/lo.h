/* Dooba SDK
 * Digital Input / Output Library
 */

#ifndef	__DIO_SCLI_LO_H
#define	__DIO_SCLI_LO_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Set Pin Low
extern void dio_scli_lo(char **args, uint16_t *args_len);

#endif
