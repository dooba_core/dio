/* Dooba SDK
 * Digital Input / Output Library
 */

#ifndef	__DIO_SCLI_INPUT_H
#define	__DIO_SCLI_INPUT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Set Pin as Input
extern void dio_scli_input(char **args, uint16_t *args_len);

#endif
