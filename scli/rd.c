/* Dooba SDK
 * Digital Input / Output Library
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "dio.h"
#include "scli/rd.h"

// Read Pin
void dio_scli_rd(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	uint8_t p;

	// Acquire Pin Number
	if(str_next_arg(args, args_len, &x, &l) == 0)				{ scli_printf("Missing pin\n"); return; }
	p = strtoul(x, 0, 0);

	// Read Pin
	scli_printf("Reading %i... ", p);
	p = dio_rd(p);
	scli_printf("%i (%s)\n", p, (p ? "High" : "Low"));
}
