/* Dooba SDK
 * Digital Input / Output Library
 */

#ifndef	__DIO_SCLI_OUTPUT_H
#define	__DIO_SCLI_OUTPUT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Set Pin as Output
extern void dio_scli_output(char **args, uint16_t *args_len);

#endif
