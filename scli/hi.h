/* Dooba SDK
 * Digital Input / Output Library
 */

#ifndef	__DIO_SCLI_HI_H
#define	__DIO_SCLI_HI_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Set Pin Low
extern void dio_scli_hi(char **args, uint16_t *args_len);

#endif
