/* Dooba SDK
 * Digital Input / Output Library
 */

#ifndef	__DIO_H
#define	__DIO_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Base Pin Count
#define	DIO_BASE_COUNT						30

// Maximum Pin Count
#define	DIO_MAX_COUNT						250

// I/O Expander Setter
typedef void (*dio_exp_set_t)(void *u, uint32_t pins);

// I/O Expander Getter
typedef uint32_t (*dio_exp_get_t)(void *u);

// I/O Expander Structure
struct dio_exp
{
	// Offset & Count
	uint8_t pin_off;
	uint8_t pin_count;

	// User Data
	void *user;

	// Current State
	uint32_t dir;
	uint32_t pullups;
	uint32_t pins;

	// Set Direction
	dio_exp_set_t set_dir;

	// Set Pull-ups
	dio_exp_set_t set_pullups;

	// Set Pins
	dio_exp_set_t set_pins;

	// Get Pins
	dio_exp_get_t get_pins;

	// Next
	struct dio_exp *next;
};

// I/O Expanders
extern struct dio_exp *dio_exps;
extern struct dio_exp *dio_exp_last;

// Pre-Initialize
extern void dio_preinit();

// Configure Pin as Input
extern void dio_input(uint8_t p);

// Configure Pin as Output
extern void dio_output(uint8_t p);

// Set Pin High
extern void dio_hi(uint8_t p);

// Set Pin Low
extern void dio_lo(uint8_t p);

// Read Pin State
extern uint8_t dio_rd(uint8_t p);

// Register I/O Expander
extern void dio_reg_exp(struct dio_exp *e, uint8_t pin_count, void *user, dio_exp_set_t set_dir, dio_exp_set_t set_pullups, dio_exp_set_t set_pins, dio_exp_get_t get_pins);

#endif
