/* Dooba SDK
 * Digital Input / Output Library
 */

// Internal Includes
#include "dio.h"

// I/O Expanders
struct dio_exp *dio_exps;
struct dio_exp *dio_exp_last;

// Pre-Initialize
void dio_preinit()
{
	// Clear Expanders
	dio_exps = 0;
	dio_exp_last = 0;
}

// Configure Pin as Input
void dio_input(uint8_t p)
{
	struct dio_exp *e;

	// Handle I/O Expanders
	if(p >= DIO_BASE_COUNT)
	{
		// Find Expander
		e = dio_exps;
		while(e)
		{
			// Check Expander
			if((p >= e->pin_off) && (p < (e->pin_off + e->pin_count)))
			{
				// Run I/O
				e->dir &= ~((uint32_t)1 << (p - e->pin_off));
				e->set_dir(e->user, e->dir);
				e->set_pullups(e->user, e->pullups);
				return;
			}

			// Next
			e = e->next;
		}

		// Done
		return;
	}

	// Switch on Pin
	switch(p)
	{
		// [0 - 7] -> Port A
		#ifdef DDRA
		case 0:  DDRA &= ~(_BV(7)); break;					// 0
		case 1:  DDRA &= ~(_BV(6)); break;					// 1
		case 2:  DDRA &= ~(_BV(5)); break;					// 2
		case 3:  DDRA &= ~(_BV(4)); break;					// 3
		case 4:  DDRA &= ~(_BV(3)); break;					// 4
		case 5:  DDRA &= ~(_BV(2)); break;					// 5
		case 6:  DDRA &= ~(_BV(1)); break;					// 6
		case 7:  DDRA &= ~(_BV(0)); break;					// 7
		#endif

		// [8 - 15] -> Port B
		#ifdef DDRB
		case 8:  DDRB &= ~(_BV(0)); break;					// 8
		case 9:  DDRB &= ~(_BV(1)); break;					// 9
		case 10: DDRB &= ~(_BV(2)); break;					// 10
		case 11: DDRB &= ~(_BV(3)); break;					// 11
		case 12: DDRB &= ~(_BV(4)); break;					// 12 - ioNode on-board LED
		case 13: DDRB &= ~(_BV(5)); break;					// 13 - SPI MOSI
		case 14: DDRB &= ~(_BV(6)); break;					// 14 - SPI MISO
		case 15: DDRB &= ~(_BV(7)); break;					// 15 - SPI SCK
		#endif

		// [16 - 21] -> Port D
		#ifdef DDRD
		case 16: DDRD &= ~(_BV(2)); break;					// 16 - UART1 RX
		case 17: DDRD &= ~(_BV(3)); break;					// 17 - UART1 TX
		case 18: DDRD &= ~(_BV(4)); break;					// 18
		case 19: DDRD &= ~(_BV(5)); break;					// 19
		case 20: DDRD &= ~(_BV(6)); break;					// 20
		case 21: DDRD &= ~(_BV(7)); break;					// 21
		#endif

		// [22 - 29] -> Port C
		#ifdef DDRC
		case 22: DDRC &= ~(_BV(0)); break;					// 22 - I2C SCL
		case 23: DDRC &= ~(_BV(1)); break;					// 23 - I2C SDA
		case 24: DDRC &= ~(_BV(2)); break;					// 24
		case 25: DDRC &= ~(_BV(3)); break;					// 25
		case 26: DDRC &= ~(_BV(4)); break;					// 26
		case 27: DDRC &= ~(_BV(5)); break;					// 27
		case 28: DDRC &= ~(_BV(6)); break;					// 28
		case 29: DDRC &= ~(_BV(7)); break;					// 29
		#endif

		// Default
		default:
			break;
	}
}

// Configure Pin as Output
void dio_output(uint8_t p)
{
	struct dio_exp *e;

	// Handle I/O Expanders
	if(p >= DIO_BASE_COUNT)
	{
		// Find Expander
		e = dio_exps;
		while(e)
		{
			// Check Expander
			if((p >= e->pin_off) && (p < (e->pin_off + e->pin_count)))
			{
				// Run I/O
				e->dir |= ((uint32_t)1 << (p - e->pin_off));
				e->set_dir(e->user, e->dir);
				e->set_pins(e->user, e->pins);
				return;
			}

			// Next
			e = e->next;
		}

		// Done
		return;
	}

	// Switch on Pin
	switch(p)
	{
		// [0 - 7] -> Port A
		#ifdef DDRA
		case 0:  DDRA |= _BV(7); break;						// 0
		case 1:  DDRA |= _BV(6); break;						// 1
		case 2:  DDRA |= _BV(5); break;						// 2
		case 3:  DDRA |= _BV(4); break;						// 3
		case 4:  DDRA |= _BV(3); break;						// 4
		case 5:  DDRA |= _BV(2); break;						// 5
		case 6:  DDRA |= _BV(1); break;						// 6
		case 7:  DDRA |= _BV(0); break;						// 7
		#endif

		// [8 - 15] -> Port B
		#ifdef DDRB
		case 8:  DDRB |= _BV(0); break;						// 8
		case 9:  DDRB |= _BV(1); break;						// 9
		case 10: DDRB |= _BV(2); break;						// 10
		case 11: DDRB |= _BV(3); break;						// 11
		case 12: DDRB |= _BV(4); break;						// 12 - ioNode on-board LED
		case 13: DDRB |= _BV(5); break;						// 13 - SPI MOSI
		case 14: DDRB |= _BV(6); break;						// 14 - SPI MISO
		case 15: DDRB |= _BV(7); break;						// 15 - SPI SCK
		#endif

		// [16 - 21] -> Port D
		#ifdef DDRD
		case 16: DDRD |= _BV(2); break;						// 16 - UART1 RX
		case 17: DDRD |= _BV(3); break;						// 17 - UART1 TX
		case 18: DDRD |= _BV(4); break;						// 18
		case 19: DDRD |= _BV(5); break;						// 19
		case 20: DDRD |= _BV(6); break;						// 20
		case 21: DDRD |= _BV(7); break;						// 21
		#endif

		// [22 - 29] -> Port C
		#ifdef DDRC
		case 22: DDRC |= _BV(0); break;						// 22 - I2C SCL
		case 23: DDRC |= _BV(1); break;						// 23 - I2C SDA
		case 24: DDRC |= _BV(2); break;						// 24
		case 25: DDRC |= _BV(3); break;						// 25
		case 26: DDRC |= _BV(4); break;						// 26
		case 27: DDRC |= _BV(5); break;						// 27
		case 28: DDRC |= _BV(6); break;						// 28
		case 29: DDRC |= _BV(7); break;						// 29
		#endif

		// Default
		default:
			break;
	}
}

// Set Pin High
void dio_hi(uint8_t p)
{
	struct dio_exp *e;
	uint32_t x;

	// Handle I/O Expanders
	if(p >= DIO_BASE_COUNT)
	{
		// Find Expander
		e = dio_exps;
		while(e)
		{
			// Check Expander
			if((p >= e->pin_off) && (p < (e->pin_off + e->pin_count)))
			{
				// Run I/O
				x = ((uint32_t)1 << (p - e->pin_off));
				if(e->dir & x)
				{
					// Set Pin
					e->pins |= x;
					e->set_pins(e->user, e->pins);
				}
				else
				{
					// Set Pull-up
					e->pullups |= x;
					e->set_pullups(e->user, e->pullups);
				}
				return;
			}

			// Next
			e = e->next;
		}

		// Done
		return;
	}

	// Switch on Pin
	switch(p)
	{
		// [0 - 7] -> Port A
		#ifdef DDRA
		case 0:  PORTA |= _BV(7); break;					// 0
		case 1:  PORTA |= _BV(6); break;					// 1
		case 2:  PORTA |= _BV(5); break;					// 2
		case 3:  PORTA |= _BV(4); break;					// 3
		case 4:  PORTA |= _BV(3); break;					// 4
		case 5:  PORTA |= _BV(2); break;					// 5
		case 6:  PORTA |= _BV(1); break;					// 6
		case 7:  PORTA |= _BV(0); break;					// 7
		#endif

		// [8 - 15] -> Port B
		#ifdef DDRB
		case 8:  PORTB |= _BV(0); break;					// 8
		case 9:  PORTB |= _BV(1); break;					// 9
		case 10: PORTB |= _BV(2); break;					// 10
		case 11: PORTB |= _BV(3); break;					// 11
		case 12: PORTB |= _BV(4); break;					// 12 - ioNode on-board LED
		case 13: PORTB |= _BV(5); break;					// 13 - SPI MOSI
		case 14: PORTB |= _BV(6); break;					// 14 - SPI MISO
		case 15: PORTB |= _BV(7); break;					// 15 - SPI SCK
		#endif

		// [16 - 21] -> Port D
		#ifdef DDRD
		case 16: PORTD |= _BV(2); break;					// 16 - UART1 RX
		case 17: PORTD |= _BV(3); break;					// 17 - UART1 TX
		case 18: PORTD |= _BV(4); break;					// 18
		case 19: PORTD |= _BV(5); break;					// 19
		case 20: PORTD |= _BV(6); break;					// 20
		case 21: PORTD |= _BV(7); break;					// 21
		#endif

		// [22 - 29] -> Port C
		#ifdef DDRC
		case 22: PORTC |= _BV(0); break;					// 22 - I2C SCL
		case 23: PORTC |= _BV(1); break;					// 23 - I2C SDA
		case 24: PORTC |= _BV(2); break;					// 24
		case 25: PORTC |= _BV(3); break;					// 25
		case 26: PORTC |= _BV(4); break;					// 26
		case 27: PORTC |= _BV(5); break;					// 27
		case 28: PORTC |= _BV(6); break;					// 28
		case 29: PORTC |= _BV(7); break;					// 29
		#endif

		// Default
		default:
			break;
	}
}

// Set Pin Low
void dio_lo(uint8_t p)
{
	struct dio_exp *e;
	uint32_t x;

	// Handle I/O Expanders
	if(p >= DIO_BASE_COUNT)
	{
		// Find Expander
		e = dio_exps;
		while(e)
		{
			// Check Expander
			if((p >= e->pin_off) && (p < (e->pin_off + e->pin_count)))
			{
				// Run I/O
				x = ((uint32_t)1 << (p - e->pin_off));
				if(e->dir & x)
				{
					// Set Pin
					e->pins &= ~x;
					e->set_pins(e->user, e->pins);
				}
				else
				{
					// Set Pull-up
					e->pullups &= ~x;
					e->set_pullups(e->user, e->pullups);
				}
				return;
			}

			// Next
			e = e->next;
		}

		// Done
		return;
	}

	// Switch on Pin
	switch(p)
	{
		// [0 - 7] -> Port A
		#ifdef DDRA
		case 0:  PORTA &= ~(_BV(7)); break;					// 0
		case 1:  PORTA &= ~(_BV(6)); break;					// 1
		case 2:  PORTA &= ~(_BV(5)); break;					// 2
		case 3:  PORTA &= ~(_BV(4)); break;					// 3
		case 4:  PORTA &= ~(_BV(3)); break;					// 4
		case 5:  PORTA &= ~(_BV(2)); break;					// 5
		case 6:  PORTA &= ~(_BV(1)); break;					// 6
		case 7:  PORTA &= ~(_BV(0)); break;					// 7
		#endif

		// [8 - 15] -> Port B
		#ifdef DDRB
		case 8:  PORTB &= ~(_BV(0)); break;					// 8
		case 9:  PORTB &= ~(_BV(1)); break;					// 9
		case 10: PORTB &= ~(_BV(2)); break;					// 10
		case 11: PORTB &= ~(_BV(3)); break;					// 11
		case 12: PORTB &= ~(_BV(4)); break;					// 12 - ioNode on-board LED
		case 13: PORTB &= ~(_BV(5)); break;					// 13 - SPI MOSI
		case 14: PORTB &= ~(_BV(6)); break;					// 14 - SPI MISO
		case 15: PORTB &= ~(_BV(7)); break;					// 15 - SPI SCK
		#endif

		// [16 - 21] -> Port D
		#ifdef DDRD
		case 16: PORTD &= ~(_BV(2)); break;					// 16 - UART1 RX
		case 17: PORTD &= ~(_BV(3)); break;					// 17 - UART1 TX
		case 18: PORTD &= ~(_BV(4)); break;					// 18
		case 19: PORTD &= ~(_BV(5)); break;					// 19
		case 20: PORTD &= ~(_BV(6)); break;					// 20
		case 21: PORTD &= ~(_BV(7)); break;					// 21
		#endif

		// [22 - 29] -> Port C
		#ifdef DDRC
		case 22: PORTC &= ~(_BV(0)); break;					// 22 - I2C SCL
		case 23: PORTC &= ~(_BV(1)); break;					// 23 - I2C SDA
		case 24: PORTC &= ~(_BV(2)); break;					// 24
		case 25: PORTC &= ~(_BV(3)); break;					// 25
		case 26: PORTC &= ~(_BV(4)); break;					// 26
		case 27: PORTC &= ~(_BV(5)); break;					// 27
		case 28: PORTC &= ~(_BV(6)); break;					// 28
		case 29: PORTC &= ~(_BV(7)); break;					// 29
		#endif

		// Default
		default:
			break;
	}
}

// Read Pin State
uint8_t dio_rd(uint8_t p)
{
	struct dio_exp *e;

	// Handle I/O Expanders
	if(p >= DIO_BASE_COUNT)
	{
		// Find Expander
		e = dio_exps;
		while(e)
		{
			// Check Expander
			if((p >= e->pin_off) && (p < (e->pin_off + e->pin_count)))
			{
				// Run I/O
				return (e->get_pins(e->user) & ((uint32_t)1 << (p - e->pin_off))) > 0;
			}

			// Next
			e = e->next;
		}

		// Done
		return 0;
	}

	// Switch on Pin
	switch(p)
	{
		// [0 - 7] -> Port A
		#ifdef DDRA
		case 0:  return (PINA & _BV(7)) > 0; break;			// 0
		case 1:  return (PINA & _BV(6)) > 0; break;			// 1
		case 2:  return (PINA & _BV(5)) > 0; break;			// 2
		case 3:  return (PINA & _BV(4)) > 0; break;			// 3
		case 4:  return (PINA & _BV(3)) > 0; break;			// 4
		case 5:  return (PINA & _BV(2)) > 0; break;			// 5
		case 6:  return (PINA & _BV(1)) > 0; break;			// 6
		case 7:  return (PINA & _BV(0)) > 0; break;			// 7
		#endif

		// [8 - 15] -> Port B
		#ifdef DDRB
		case 8:  return (PINB & _BV(0)) > 0; break;			// 8
		case 9:  return (PINB & _BV(1)) > 0; break;			// 9
		case 10: return (PINB & _BV(2)) > 0; break;			// 10
		case 11: return (PINB & _BV(3)) > 0; break;			// 11
		case 12: return (PINB & _BV(4)) > 0; break;			// 12 - ioNode on-board LED
		case 13: return (PINB & _BV(5)) > 0; break;			// 13 - SPI MOSI
		case 14: return (PINB & _BV(6)) > 0; break;			// 14 - SPI MISO
		case 15: return (PINB & _BV(7)) > 0; break;			// 15 - SPI SCK
		#endif

		// [16 - 21] -> Port D
		#ifdef DDRD
		case 16: return (PIND & _BV(2)) > 0; break;			// 16 - UART1 RX
		case 17: return (PIND & _BV(3)) > 0; break;			// 17 - UART1 TX
		case 18: return (PIND & _BV(4)) > 0; break;			// 18
		case 19: return (PIND & _BV(5)) > 0; break;			// 19
		case 20: return (PIND & _BV(6)) > 0; break;			// 20
		case 21: return (PIND & _BV(7)) > 0; break;			// 21
		#endif

		// [22 - 29] -> Port C
		#ifdef DDRC
		case 22: return (PINC & _BV(0)) > 0; break;			// 22 - I2C SCL
		case 23: return (PINC & _BV(1)) > 0; break;			// 23 - I2C SDA
		case 24: return (PINC & _BV(2)) > 0; break;			// 24
		case 25: return (PINC & _BV(3)) > 0; break;			// 25
		case 26: return (PINC & _BV(4)) > 0; break;			// 26
		case 27: return (PINC & _BV(5)) > 0; break;			// 27
		case 28: return (PINC & _BV(6)) > 0; break;			// 28
		case 29: return (PINC & _BV(7)) > 0; break;			// 29
		#endif

		// Default
		default:
			break;
	}

	return 0;
}

// Register I/O Expander
void dio_reg_exp(struct dio_exp *e, uint8_t pin_count, void *user, dio_exp_set_t set_dir, dio_exp_set_t set_pullups, dio_exp_set_t set_pins, dio_exp_get_t get_pins)
{
	uint16_t n;

	// Determine Next Pin Offset
	n = dio_exps ? (dio_exp_last->pin_off + dio_exp_last->pin_count) : DIO_BASE_COUNT;
	if((n + pin_count) > DIO_MAX_COUNT)														{ return; }

	// Write Meth (Setup Expander)
	e->pin_off = n;
	e->pin_count = pin_count;
	e->user = user;
	e->dir = 0;
	e->pullups = 0;
	e->pins = 0;
	e->set_dir = set_dir;
	e->set_pullups = set_pullups;
	e->set_pins = set_pins;
	e->get_pins = get_pins;
	e->set_dir(user, e->dir);
	e->set_pullups(user, e->pullups);
	e->set_pins(user, e->pins);
	e->next = 0;

	// Insert
	if(dio_exps)																			{ dio_exp_last->next = e; }
	else																					{ dio_exps = e; }
	dio_exp_last = e;
}
